#include "forme.h"

void ConstruireForme(Forme_t * this)
{
    this = (Forme_t *)malloc(sizeof(Forme_t));
    LaMetaForme.constructeurObjetGraphique(this);
}

void initMetaForme()
{
    LaMetaForme.setY = &setY;
    LaMetaForme.setX = &setX; 
    LaMetaForme.getY = &getY;
    LaMetaForme.getX = &getX; 
    LaMetaForme.constructeurObjetGraphique = &constructeurObjetGraphique;
    LaMetaForme.nbObjetsGraphiques = 0;
    LaMetaForme.getNbObjetsGraphiques = &getNbObjetsGraphiques;

}

void setX(int x, struct Forme * this){this->_x = x;}
int getX(struct Forme * this){return this->_x;}
void setY(int y, struct Forme * this){this->_y = y;}
int getY(struct Forme * this){return this->_y;}
int getNbObjetsGraphiques(){return LaMetaForme.nbObjetsGraphiques;}

void constructeurObjetGraphique(struct Forme * this)
{
    this->_metaForme = &LaMetaForme;

    this->_metaForme->setX(0, this);
    this->_metaForme->setY(0, this);

    (this->_metaForme->nbObjetsGraphiques)++;
}