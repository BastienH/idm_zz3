#ifndef CERCLE_H
#define CERCLE_H

#include "forme.h"

typedef struct Cercle
{
    struct Forme * _superClasse;
    struct MetaCercle * _metaCercle;

    int _rayon;

} Cercle_t;

void ConstruireCercle(Cercle_t * this);

typedef struct MetaCercle
{   
    MetaForme_t * _superMetaClasse;

    void (*setRayon)(int x, struct Cercle * this);
    int (*getRayon)(struct Cercle * this);

    void (*constructeurCercle)(struct Cercle * this);

} MetaCercle_t;

MetaCercle_t LeMetaCercle;

void initMetaCercle();


void setRayon(int y, struct Cercle * this);
int getRayon(struct Cercle * this);
void constructeurCercle(struct Cercle * this);


#endif