#ifndef FORME_H
#define FORME_H

#include <stdio.h>
#include <stdlib.h>

typedef struct Forme
{
   struct MetaForme * _metaForme;

   int _x;
   int _y;  

} Forme_t;

void ConstruireForme(Forme_t * this);

typedef struct MetaForme
{   
    void (*setX)(int x, struct Forme * this);
    int (*getX)(struct Forme * this);
    void (*setY)(int y, struct Forme * this);
    int (*getY)(struct Forme * this);

    int nbObjetsGraphiques;

    int (*getNbObjetsGraphiques)();

    void (*constructeurObjetGraphique)(struct Forme * this);

} MetaForme_t;

MetaForme_t LaMetaForme;

void initMetaForme();


void setX(int x, struct Forme * this);
int getX(struct Forme * this);
void setY(int y, struct Forme * this);
int getY(struct Forme * this);
void constructeurObjetGraphique(struct Forme * this);
int getNbObjetsGraphiques();

#endif