#include "cercle.h"


void ConstruireCercle(Cercle_t * this)
{
    this = (Cercle_t *)malloc(sizeof(Cercle_t));
    LeMetaCercle.constructeurCercle(this);
}

void initMetaCercle()
{
    LeMetaCercle.setRayon = &setRayon;
    LeMetaCercle.getRayon = &getRayon; 
   
    LeMetaCercle.constructeurCercle = &constructeurCercle;

}

void setRayon(int r, struct Cercle * this){this->_rayon = r;}
int getRayon(struct Cercle * this){return this->_rayon;}


void constructeurCercle(struct Cercle * this)
{
    this->_metaCercle = &LeMetaCercle;

    Forme_t * forme;

    ConstruireForme(forme);

    this->_superClasse = forme;

    this->_metaCercle->_superMetaClasse = &LaMetaForme;

    this->_metaCercle->_superMetaClasse->constructeurObjetGraphique(this->_superClasse);

    this->_metaCercle->setRayon(0, this);
}