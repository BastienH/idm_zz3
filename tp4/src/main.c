#include "rectangle.h"
#include "cercle.h"


int main()
{
    initMetaForme();
    initMetaCercle();

    Forme_t forme;


    ConstruireForme(&forme);


    LaMetaForme.setX(5, &forme);

    printf("%d\n", LaMetaForme.getX(&forme));
    printf("%d\n", LaMetaForme.getNbObjetsGraphiques());

    Cercle_t cercle;
    ConstruireCercle(&cercle);

    printf("%d\n", cercle._metaCercle->_superMetaClasse->getX(cercle._superClasse));
    return 0;
}