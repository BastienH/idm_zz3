

#include "pi.hpp"
#include <iostream>
#include <ctime>

int main (int argc, char ** argv)
{
    if(argc > 1)
    {
        std::cout << calculPiFromFic(argv[1]) << std::endl;
    }
    else
    {
        //generateStatus();
        
        clock_t t1, t2;

        /*t1 = clock();

        calculPi();

        t2 = clock();

        std::cout << "Sans threads : " << (float)(t2 - t1)/CLOCKS_PER_SEC << std::endl;*/



        t1 = clock();

        calculPiThread();
        
        t2 = clock();

        std::cout << "Avec threads : " <<  (float)(t2 - t1)/CLOCKS_PER_SEC << std::endl;

    }
    return 0;
}