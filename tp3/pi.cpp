
#include "pi.hpp"

#include <thread>
#include <array>

double calculPi()
{
    double x, y;
    unsigned long cpt = 0;
    double pi = 0;

    for(int i = 0; i < 10; i++)
    {
        calculOnePi(i, &pi);
    }
    
    return pi/10;
}

double calculPiThread()
{
    double pi = 0;

    int nbThreads = 10;
    unsigned long iterMax = 1000000000;

    std::array<std::thread, 10> threads;

    for(int i = 0; i < nbThreads; i++)
    {
        threads[i] = std::thread(calculOnePi, i, &pi); 
    }

    for(auto i = threads.begin(); i != threads.end(); i++)
    {
        i->join();
    }
    
    
    return pi/nbThreads;
    
}

void generateStatus()
{
    CLHEP::MTwistEngine mtRng = CLHEP::MTwistEngine();

    double fRn;

    for(int i = 0; i < 10; i++)
    {
        for(int j = 0; j < 2000000000; j++)
        {
            fRn = mtRng.flat(); // Génération uniforme avec MT
        }
        
        std::string str = "./Status/Status_" + std::to_string(i) + ".txt";

        mtRng.saveStatus(str.c_str());

    }
}

void calculOnePi(int numStatus, double * pi)
{
    CLHEP::MTwistEngine mtRng;    

    double x, y;
    unsigned long cpt = 0;

    unsigned long iterMax = 1000000000;

    std::cout << "Start : " << numStatus << std::endl;

    std::string str = "./Status/Status_" + std::to_string(numStatus) + ".txt";
    
    mtRng.restoreStatus(str.c_str());

    for(int j = 0; j < iterMax; j++)
    {
        x = mtRng.flat(); // Génération uniforme avec MT
        y = mtRng.flat(); // Génération uniforme avec MT


        if(x*x + y*y < 1)
        {
            ++cpt;
        }

    }

    std::cout << 4*(float)cpt/iterMax << std::endl;

    *pi += 4*(float)cpt/iterMax;

}

double calculPiFromFic(const std::string& ficName)
{
    CLHEP::MTwistEngine mtRng = CLHEP::MTwistEngine();

    double x, y;
    unsigned long cpt = 0;

    unsigned long iterMax = 1000000000;

  
    mtRng.restoreStatus(ficName.c_str());

    for(int j = 0; j < iterMax; j++)
    {
        x = mtRng.flat(); // Génération uniforme avec MT
        y = mtRng.flat(); // Génération uniforme avec MT


        if(x*x + y*y < 1)
        {
            ++cpt;
        }

    }

    return 4*(float)cpt/iterMax;
   
}