#ifndef PI_HPP
#define PI_HPP

#include <cmath>
#include <iostream>

// Générer des nombres dans un fichier binaire (pour test DIE HARD de G. Marsaglia)
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <unistd.h>
#include <string>
#include "Random/CLHEP/Random/MTwistEngine.h" // Entête pour MT dans la bibliothèque CLHEP


double calculPi();

double calculPiThread();

void generateStatus();

void calculOnePi(int numStatus, double * pi);

double calculPiFromFic(const std::string& ficName);

#endif