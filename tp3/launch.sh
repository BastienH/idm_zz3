#!/bin/bash


pref="./build/Status/Status_"
suf=".txt"

for i in `seq 0 9`;
do
    name=($pref$i$suf)
    #echo $name
    
    ./build/q2 $name >> pi.txt & 
done

PIDS=`ps | grep q2`

#echo "$PIDS"

while [ -n "$PIDS" ]; do
    PIDS=`ps | grep q2`
    sleep 0.1
done
