cmake_minimum_required(VERSION 3.0)

project(tp6)

include_directories(src)


link_libraries(pthread)

set(SRC 
        src/main.cpp
    )

add_compile_options("-Wall")
add_compile_options("-g")

add_executable(${CMAKE_PROJECT_NAME} ${SRC})

add_executable(test src/maintest.cpp)
