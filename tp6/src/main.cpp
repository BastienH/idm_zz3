
#include <iostream>
#include <vector>
#include <random>
#include <iomanip>
#include <algorithm>

const int N = 10000000;

template<typename T>
T classicalSum(const std::vector<T>& v)
{
    T sum = 0.0;
    for(auto val : v)
    {
        sum += val;
    }

    return sum;
}

template<typename T>
T kahanSum(const std::vector<T>& v)
{
    T sum = 0.0;
    T c = 0.0;
    for(auto val : v)
    {
        T y = val - c;
        T t = sum + y;
        c = (t - sum) - y;
        sum = t;
    }

    return sum;
}


template<typename T>
T pairSum(std::vector<T>& v, typename std::vector<T>::iterator begin, typename  std::vector<T>::iterator end, int size)
{

    if(size < 20)
    {
        T res = 0.0;
        
        for(auto it = begin; it < end; ++it)
        {
            res += *it;
        }

        return res;
    }
    else
    {
        const std::size_t m = size/2;

        typename std::vector<T>::iterator middle = begin + m;

        return pairSum(v, begin, middle, m) + pairSum(v, middle, end, m);
    }
    
}


template<typename T> 
void generateNumbers(bool sum, bool kahan, bool pair)
{
    std::vector<T> v;

    for(unsigned long int i = 0; i < N; ++i)
    {
        v.push_back(3.14159265359);
    }

    std::cout << std::setprecision(18);

    long double real = 31415926.5359L;
    std::cout << "real : " << real << std::endl;

    if(sum)
    {
        T sum = classicalSum(v);
        std::cout << "Sum : "  << sum << std::endl;
        std::cout << "Sum relative error : " << fabs((sum - real) / real) << std::endl;
    }

    if(kahan)
    {
        T kSum = kahanSum(v);
        std::cout << "Kahan : " << kSum << std::endl;
        std::cout << "Kahan sum relative error : " << fabs((kSum - real) / real) << std::endl;
    }

    if(pair)
    {
        T pSum = pairSum<T>(v, v.begin(), v.end(), N);
        std::cout << "Pair : " << pSum << std::endl;
        std::cout << "Pair sum relative error : " << fabs((pSum - real) / real) << std::endl;
    }


}

template<typename T>
void generateRandomVector()
{
    std::vector<T> v;

    std::mt19937 gen(1); 
    std::uniform_real_distribution<T> dis(0.0, 5.0);

    std::cout << std::setprecision(18);

    for(unsigned long int i = 0; i < N; ++i)
    {
        v.push_back(dis(gen));
    }
    

    for(int j = 0; j < 10; ++j)
    {
        

        T sum = classicalSum(v);
        std::cout << sum;

        T kSum = kahanSum(v);
        std::cout << " " << kSum;

        T pSum = pairSum<T>(v, v.begin(), v.end(), N);
        std::cout << " " << pSum << std::endl;
  
        std::shuffle(v.begin(), v.end(), gen);
    }

}


int main()
{
    //std::cout << "float" << std::endl;
    //generateNumbers<float>(true, false, false);
    //generateNumbers<float>(false, true, false);
    //generateNumbers<float>(false, false, true);

    //std::cout << "double" << std::endl;
    //generateNumbers<double>(true, false, false);
    //generateNumbers<double>(false, true, false);
    //generateNumbers<double>(false, false, true);


    //std::cout << "long double" << std::endl;
    //generateNumbers<long double>(true, false, false);
    //generateNumbers<long double>(false, true, false);
    //generateNumbers<long double>(false, false, true);


    //generateRandomVector<long double>();

}

