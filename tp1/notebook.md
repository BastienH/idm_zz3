# TP1

## 1.En vous aidant des ressources sur Internet, proposer des définitions pour les concepts de métadonnée, d'introspection, de méta-programmation et de réflexivité.Décrire leurs articulations possibles.

### Métadonnée : 

    Donnée servant à caractériser une autre donnée.
    Src : https://www.larousse.fr/dictionnaires/francais/m%C3%A9tadonn%C3%A9e/186919

    Par exemple les métadonnées les plus courantes sont la date de sauvegarde, la taille et l’auteur du fichier… toute information permettant d’identifier et localiser les données 
    Src : https://www.dataengineers.eu/fr/les-metadonnees-pour-les-nuls/

### Introspection :

    Observation méthodique, par le sujet lui-même, de ses états de conscience et de sa vie intérieure.
    Src : https://www.larousse.fr/dictionnaires/francais/introspection/44019

    l'introspection, qui est la capacité d'un programme à examiner son propre état ;
    Src : https://fr.wikipedia.org/wiki/R%C3%A9flexion_(informatique)

### Méta-programmation :

    La métaprogrammation, désigne l'écriture de programmes qui manipulent des données décrivant elles-mêmes des programmes. Dans le cas particulier où le programme manipule ses propres instructions pendant son exécution, on parle de programme auto-modifiant. 
    Src : https://fr.wikipedia.org/wiki/M%C3%A9taprogrammation

### Réflexivité :

    On appelle réflexivité le fait pour un langage de programmation de permettre l'écriture de tels programmes. Un tel langage de programmation est dit réflexif. 
    Src : https://fr.wikipedia.org/wiki/R%C3%A9flexion_(informatique)


![alt text](articulation.png "Articulation des différents concepts")

## 2.Rechercher ce que signifie «l'ingénierie des modèles.»

    L’ingénierie dirigée par les modèles (IDM), ou Model Driven Engineering (MDE) en anglais, a permis plusieurs améliorations significatives dans le développement de systèmes complexes enpermettant de se concentrer sur une préoccupation plus abstraite que la programmation classique (Modèle objet, ...). Il s’agit d’une forme d’ingénierie générative dans laquelle tout ou partie d’une application estengendrée à partir de modèles. 
    
    Un modèle est une abstraction, une simplification d’un système qui est suffisante pour comprendre le système modélisé et répondre aux questions que l’on se pose sur lui. (Principe de substituabilité)

    Src : https://hal.archives-ouvertes.fr/hal-00371565/document


## 3. En  ce  qui  concerne  les  métadonnées,  regarder  dans  un  1er temps  le  concept  des  annotations  dans  le langage Java. L’introduction des annotations s’est effectuée historiquement à partir de la version 1.5 –voir la synthèse dans lien ci-dessous sur l’ancien site de référence de Sun pour le langage Java : 

## http://java.sun.com/j2se/1.5.0/docs/guide/language/annotations.html

## Vous complèterez  avec des URLs récentes Java 13 sept. 2019(et 14 – juin 2020) et suivants ou tout autre site Web que vous jugerez utiles. Consultez également les sites sur l'Ingénierie Des Modèles, le MDA etc (le site de l'OMG) et en français (Actions  spécifiques  du  CNRS – «Model  Driven  Engineering» ou d’autres sources que vous jugerez pertinentes).



## 4. Un article de recherche a utilisé les techniques de meta-programmation dès l’introduction de l’introspection en Java. Etudier la technique proposée (PDF de l’article sur l’url du cours ~.../IDM-ZZ3), elle repose sur l’utilisation de l’ingénierie  des  modèles. Dès l’introduction des annotations, ce type d’outil a permis la  production automatique d'interfaces utilisateur pour des logiciels de simulations et ce grâce à l’introspection de métadonnées (en Java dès l’introduction des annotations).


## 5. D’autres langages que  Java offrent-ils un  concept  équivalent?Essayer de trouver  des  discussions  sur leurs atouts et faiblesses. 

    .Net (attributs.Net)
    Java 
    PHP
    Python (decorator)


### Avantages

    - Les annotations permettent aux programmeurs de déclarer dans leur code source comment leurs programmes doivent se comporter. C'est un exemple de construction déclarative.
    - Elles suivent le principe DRY (don't repeat yourself), regroupant ainsi les méta-données avec le code auxquelles elles s'appliquent. - Les méta-données restent normalement toujours synchronisées avec le code.
    - Elles peuvent être générées automatiquement (ex : par Javadoc).

### Inconvénients

    - L'ajout de méta-données dans un fichier compilé ou dans la mémoire à l'exécution requiert des ressources mémoire additionnelles.
    - Il y a peu de standards dictant comment ces méta-données doivent être employées.

## 6. Considérer les limites de la RTTI (Run Time Type Interface) du C++. Regardez ce qu’est la RTTI si vous ne connaissez  pas encore).

### RTTI (Run-Time Type Information) : 
    Est utilisé pour signaler la capacité d'un langage de programmation à déterminer le type d'une variable pendant l'exécution d'un programme. 

##  Que proposeriez-vous pour intégrer et  mettre simplement en œuvre dans le langage C++ par exemple, les concepts plus évolués que vous avez découvert dans les points 1 à 3.

    L'utilisation des templates
