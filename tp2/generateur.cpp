#include "generateur.hpp"
#include "mersenne.h"

#include <string>
#include <iostream>
#include <fstream>

void generateC(const unsigned int& size)
{

    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);

    std::string line = "double tabMT_1[] = { ";

    for(unsigned int i = 0; i < size; ++i)
    {
        line +=  std::to_string(genrand_real2());

        if(i < size - 1)
            line += ", ";
    }

    line += "};";

    std::ofstream myfile;
    myfile.open ("../c_code.h");
    myfile << line;
    myfile.close();

    //std::cout << line << std::endl;

}