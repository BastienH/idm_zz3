#include "pi.hpp"
#include "mersenne.h"
#include <cmath>
#include <iostream>

#include "generateur.hpp"

double calculPi(const unsigned int& iterMax)
{
    double x, y;
    unsigned long cpt = 0;

    for(unsigned int i = 0; i < iterMax; ++i)
    {
        x = genrand_real2();
        y = genrand_real2();

        if(x*x + y*y < 1)
        {
            ++cpt;
        }

    }
    
    return 4*(float)cpt/iterMax;
}



void statSeq(const unsigned long& nbPoints, const int& nbRepet)
{
    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);

    double moy = 0;
    double ecart_moy = 0;
    double temp;

    for(int i = 0; i < nbRepet; ++i)
    {
        temp = calculPi(nbPoints);
        moy += temp;
        ecart_moy += std::abs(M_PI - temp);
    }

    std::cout << "Moyenne : " << moy/nbRepet << " Ecart moyen à " <<  M_PI << " : " << ecart_moy/nbRepet << std::endl;

}

#include "c_code.h"

void calculPiFromTab(const unsigned int& nb_pts, const int& nbRepet)
{
    double x, y;
    int indice = 0;

    
    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);

    double moy = 0;
    double ecart_moy = 0;
    double temp;
    
    for(int i = 0; i < nbRepet; ++i)
    {
    
        unsigned long cpt = 0;
        
        for(unsigned int j = 0; j < nb_pts; ++j)
        {
            x = tabMT_1[indice];
            ++indice;
            y = tabMT_1[indice];
            ++indice;

            if(x*x + y*y <= 1)
            {
                ++cpt;
            }

        }

        temp = 4*(float)cpt/nb_pts;
        moy += temp;
        ecart_moy += std::abs(M_PI - temp);

    }
    
    std::cout << "Moyenne : " << moy/nbRepet << " Ecart moyen à " <<  M_PI << " : " << ecart_moy/nbRepet << std::endl;
}

