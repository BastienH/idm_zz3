#ifndef PI_HPP
#define PI_HPP

double calculPi(const unsigned int& iterMax);

void statSeq(const unsigned long& nbPoints, const int& nbRepet);

void calculPiFromTab(const unsigned int& iterMax, const int& nbRepet);
#endif