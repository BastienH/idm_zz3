#include "generateur.hpp"
#include "pi.hpp"

#include <iostream>
#include <ctime>

int main(int argc, char const *argv[])
{
    clock_t t1, t2;

    generateC(2 * 10 * 100000);


    t1 = clock();

    statSeq(100000, 10);

    t2 = clock();

    std::cout << (float)(t2 - t1)/CLOCKS_PER_SEC << std::endl;

    t1 = clock();

    calculPiFromTab(100000, 10);
    
    t2 = clock();

    std::cout << (float)(t2 - t1)/CLOCKS_PER_SEC << std::endl;

    return 0;
}

/*
Moyenne : 3.14476 Ecart moyen à 3.14159 : 0.00368441
0.046875

Moyenne : 3.14476 Ecart moyen à 3.14159 : 0.0036804
0.015625
*/