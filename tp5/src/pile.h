#ifndef PILE_H
#define PILE_H

#include <stdlib.h>

#define TAILLE_MAX 5
typedef enum{FAUX, VRAI} bool;

#define DECLARER_PILE(TYPE)                                     \
typedef struct MetaPile##TYPE                                   \
{                                                               \                
    void (*Construire)(struct Pile##TYPE* this, int taille);    \
    void (*Detruire)(struct Pile##TYPE* this);                  \
    void (*empiler)(TYPE valeur, struct Pile##TYPE* this);      \
    TYPE (*depiler)(struct Pile##TYPE* this);                   \
    bool (*estVide)(struct Pile##TYPE* this);                   \
    TYPE (*sommet)(struct Pile##TYPE* this);                    \
}MetaPile##TYPE##_t;                                            \
                                                                \
struct MetaPile##TYPE LaMetaPile##TYPE;                         \
                                                                \
typedef struct Pile##TYPE                                       \
{                                                               \                
    TYPE* pile;                                                 \
    TYPE* top;                                                  \
    struct MetaPile##TYPE * myClass;                            \
}Pile##TYPE##_t;                                                \
                                                                \
void empiler##TYPE(TYPE valeur, struct Pile##TYPE* this);       \
TYPE depiler##TYPE(struct Pile##TYPE* this);                    \
bool estVide##TYPE(struct Pile##TYPE* this);                    \
TYPE sommet##TYPE(struct Pile##TYPE* this);                     \
void Construire##TYPE(struct Pile##TYPE* this, int taille);     \
void Detruire##TYPE(struct Pile##TYPE* this);                   \
                                                                \
void initMetaPile##TYPE()                                       \
{                                                               \
    LaMetaPile##TYPE.Construire = &Construire##TYPE;            \
    LaMetaPile##TYPE.Detruire = &Detruire##TYPE;                \
    LaMetaPile##TYPE.empiler = &empiler##TYPE;                  \
    LaMetaPile##TYPE.depiler = &depiler##TYPE;                  \
    LaMetaPile##TYPE.sommet = &sommet##TYPE;                    \
    LaMetaPile##TYPE.estVide = &estVide##TYPE;                  \
}                                                               \

#define IMPLEMENTER_PILE(TYPE)                                  \
void empiler##TYPE(TYPE valeur, struct Pile##TYPE* this)        \
{                                                               \
    if(this->top == NULL)                                       \
    {                                                           \
        this->top = this->pile;                                 \
    }                                                           \
    else                                                        \
    {                                                           \
        ++(this->top);                                          \
    }                                                           \
                                                                \
    *(this->top) = valeur;                                      \
}                                                               \
                                                                \
TYPE depiler##TYPE(struct Pile##TYPE* this)                     \
{                                                               \
    TYPE res = *(this->top);                                    \
    --(this->top);                                              \
                                                                \
    return res;                                                 \
}                                                               \
                                                                \
bool estVide##TYPE(struct Pile##TYPE* this)                     \
{                                                               \
    return this->top == NULL;                                   \
}                                                               \
                                                                \
TYPE sommet##TYPE(struct Pile##TYPE* this)                      \
{                                                               \
    TYPE res = *(this->top);                                    \
    return res;                                                 \
}                                                               \
                                                                \
void Construire##TYPE(struct Pile##TYPE* this, int taille)      \
{                                                               \
    this->top = NULL;                                           \
    this->pile = (TYPE*)malloc(taille * sizeof(TYPE));          \
    this->myClass =  &LaMetaPile##TYPE;                         \
}                                                               \
                                                                \
void Detruire##TYPE(struct Pile##TYPE* this)                    \
{                                                               \
    this->top = NULL;                                           \
    free(this->pile);                                           \
}                                                               \

#endif