#include "pile.h"

#include <stdio.h>

DECLARER_PILE(int)

IMPLEMENTER_PILE(int)


int main()
{
    initMetaPileint();

    Pileint_t p;

    LaMetaPileint.Construire(&p, 10);

    p.myClass->empiler(5, &p);

    printf("%d\n", p.myClass->sommet(&p));
    return 0;
}